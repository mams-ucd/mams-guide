# mams-guide

This repository contains a set of Multi-Agent MicroServices (MAMS) projects that will help you get started with MAMS. MAMS uses the [ASTRA programming language](https://gitlab.com/astra-language) and there is a getting started guide [here](http://guide.astralanguage.com). You will need to understand how ASTRA works before you can follow this guide.

This guide will be periodically updated to reflect the current release of MAMS.  Each release will be tagged based on the version of MAMS.

The current version of this guide is for `MAMS version 1.0.10`.

## Guide Sections

* [MAMS Java-Types](javatypes): This is the first library that was developed for MAMS. It uses Java classes as schema for resources.  This supports both individual resources and collections of resources that can be manipulated using basic HTTP operations.

