# MAMS Java-Types

The MAMS Java-Types library is the simplest library that can be used to create Resources for MAMS agents.  It supports only JSON resource representations that use Java classes as Schema for resource representations.  The library supports both individual resources and homogeneous lists of resources.

To read about other MAMS libraries, please return to the [Main Page](https://gitlab.com/mams-ucd/mams-guide/).

## Individual Resources

Individual Resources can be created by the agent. They cannot have children and respond as follows to HTTP Requests:

| HTTP Method | Description                                                           | Outcome                             |
|-------------|-----------------------------------------------------------------------|-------------------------------------|
| GET         | Retrieve a representation of the state of the resource                | `200 OK`                            |
|             |                                                                       | JSON Representation of the resource |
| PUT         | Replace the current resource state with a new state                   | `200 OK`                            |
|             | (takes a JSON representation of the new state in body in the request) |                                     |

All other operations are `403 Forbidden`.

## List Resources

List Resources can be created by the agent. They have no direct state but can have children. They respond as follows to HTTP Requests:

| HTTP Method | Description                                                           | Outcome                                                   |
|-------------|-----------------------------------------------------------------------|-----------------------------------------------------------|
| GET         | Retrieve a representation of the state of the list                    | `200 OK`                                                  |
|             |                                                                       | JSON Representation of the resource                       |
| POST        | Add a new child resource to the list                                  | `201 Created`                                             |
|             | (takes a JSON representation of the new state in body in the request) | `Location` header contains URL of newly created resource  |

All other operations are `403 Forbidden`.

## List Item Resources

List Item Resources can be created by the agent or via a post request to a List Resource (see above0). They cannot have children and respond as follows to HTTP Requests:

| HTTP Method | Description                                                           | Outcome                             |
|-------------|-----------------------------------------------------------------------|-------------------------------------|
| GET         | Retrieve a representation of the state of the resource                | `200 OK`                            |
|             |                                                                       | JSON Representation of the resource |
| PUT         | Replace the current resource state with a new state                   | `200 OK`                            |
|             | (takes a JSON representation of the new state in body in the request) |                                     |
| DELETE      | Delete the resource (from the list)                                   | `200 OK`                            |
|             |                                                                       | JSON Representation of the resource |

All other operations are `403 Forbidden`.

## Implementation Strategies

MAMS includes two main implementation strategies:

* [The Passive Resource Model](passive): Here the agent creates resources but does not control access to those resources (it simply receives updates on changes to the resource)
* [The Active Resource Model](active): Here the agent plays an active role in controlling access to the resources.

*Note: MAMS allows you to mix and match models, so a MAMS agent can include both passive and active resources.*
