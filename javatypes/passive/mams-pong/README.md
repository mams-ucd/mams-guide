# mams-pong

This project demonstrates how agents can interact indirectly using HTTP. It builds upon the first two examples:

* [mams-hello](../mams-hello/README.md) demonstrates how to write agents that have resources.
* [mams-helloagent](../mams-helloagent/README.md) demonstrates how to write agents that can interact with resources.

This example consists of 3 agents: a `Main` agent that sets up the environment; a `Pinger` agent that is able to send `ping` messages and a `Ponger` agent that is able to send `pong` messages. The idea is that the `Pinger` agent sends `ping` messages to the `Ponger` agent in the form of HTTP POST Requests.  The `Ponger` agent sends `pong` messages back to the `Pinger` in the same way.

You can go back to the main page by clicking [here](../README.md).

## Project structure

This is a [Maven](https://maven.apache.org) project that uses the ASTRA Maven Plugin.  The project is described in the `pom.xml` file. The ASTRA source code (`Main.astra`, `Pinger.astra` and `Ponger.astra`) is in the `src/main/astra` folder, while the Java class `Message.java` used to specify the resources is in the `src/main/java` folder.


## Compiling and Running the code

To compile and run this example, you should be in the `mams-pong` folder.

To compile the code, type:

```
mvn compile
```

To run the code, type:

```
mvn astra:deploy
```

The expected output should look something like this:

```
[pinger]creating: pinger-base
Creating base artifact: pinger
[ponger]creating: ponger-base
Creating base artifact: ponger
Exposing Agent @ Base Uri: http://137.43.154.89:9000/pinger
Exposing Agent @ Base Uri: http://137.43.154.89:9000/ponger
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[Web Server] Method: POST / Action: /pinger/pings
[Web Server] Method: POST / Action: /ponger/pongs
[ponger] 10 PONGS!!!
```

You may have to use `CTRL-C` to stop the program after this line.

## Deconstructing the example

The code for this example is spread across three ASTRA files: `Main.astra`, `Pinger.astra` and `Ponger.astra`. The initial behaviour is described in the `Main.astra` file:

```
agent Main extends mams.MAMSAgent {
    rule +!main(list args) {
        MAMSAgent::!setup();

        S.createAgent("pinger", "Pinger");
        S.createAgent("ponger", "Ponger");
        S.setMainGoal("ponger", ["http://localhost:9000/pinger"]);
    }
}
```

Basically, the `Main` agent sets up the MAMS environment and then creates two agents: a `pinger` and a `ponger`.  Finally, is sets the main goal fo the `ponger`, passing the URI of the `pinger` as an argument.  This URI has been hardcoded, but it is relatively trivial to extend the code so that the `pinger` sends its URI to the `Main` agent which then creates the `ponger` agent. A sketch of what this would look like is given at the end of this guide.

### The Pinger agent


The `pinger` agent is responsible for receiving `ping` messages and sending `pong` messages.  Because it creates a resource, the `Pinger` agent extends the `mams.javatypes.Passive` agent class. It also includes the `mams.JSONBuilder` module to help with the creation of the body of HTTP Requests.
```
agent Pinger extends mams.javatypes.Passive {
    module mams.JSONBuilder builder;

    constant string TYPE = "Message";
    constant string LIST_NAME = "pings";
    constant string URI = "uri";
}
```

The three constants are included to increase the readability of the code.

The initial behaviour of this agent is associated with the `!init()` goal:

```
initial !init();

rule +!init() {
    MAMSAgent::!init();
    MAMSAgent::!created("base");

    Passive::!listResource(LIST_NAME, TYPE);
}
```
This is an alternative way of forcing initial behaviour on the agent.  The `!init()` goal is declared at startup (via the `initial` keyword) and the rule is used to handle the adoption of that goal.  Here, the code is very similar to the [mams-hello](../mams-hello/README.md) agent code.  The first line connects the agent to the MAMS environment, the second line creates a Hypermedia body for the agent and the third line creates a list resource with name `pings` of type `Message`.

The main behaviour of this agent is implemented via the `+itemResource(...)` event:
```
rule +itemResource(string artifact_name, TYPE) {
    !itemProperty(artifact_name, URI, funct value);
    !pinged(F.valueAsString(value, 0));
}
```
This belief adoption event is generated whenever a new resource is added to the `pings` list.  The first line retrieves the `uri` property of the newly created resource and the second line drives the expected response (the `pong` message):

```
    rule +!pinged(string pongerUri) :
            artifact(LIST_NAME, string artifact_name, cartago.ArtifactId id) &
            uri(artifact_name, string pinger_uri) {
        JsonNode node = builder.createObject();
        builder.addProperty(node, URI, pinger_uri);

        mams.MAMSAgent::!post(pongerUri,
            builder.toJsonString(node),
            HttpResponse response
        );

        if (httpUtils.hasCode(response, 201)) {
            string uri = httpUtils.header(response, "Location");
        } else {
            S.fail();
        }
    }
```

The body of this rule is very similar to the Create behaviour specified in [mams-helloagent](../mams-helloagent/README.md) code.  It is just adapted to the `Message` Java type. The main novelty of this piece of code is the context, which is used to get the URI of the `pings` list resource.  This is needed because this information is sent in the `pong` message. The context is extracted below:

```
artifact(LIST_NAME, string artifact_name, cartago.ArtifactId id) &
uri(artifact_name, string pinger_uri)
```

The first formula is used to get the `artifact_name` and `id` for the `pings` list artifact (this is the way the `pings` resource is implemented).  Thre `artifact_name` is used to get the URI of the resource via the second formula (`pinger_uri`). Looking at the body of the code, this URI is embedded in the JSON object send as part of the POST Request that implements the `pong` message.

### The Ponger Agent
The `Ponger` agent is very similar to the `Pinger` agent with the exception that it includes a mechanism to stop the `Ping-Pong` behaviour.  This is done by creating a `pongs` belief that takes an integer value (initially 0):

```
types ponger {
    formula pongs(int);
}

initial pongs(0);
```

To force the agent to stop ending `pong` messages, the `+itemResource(...)` event is overridden:

```
rule +itemResource(string artifact_name, TYPE) : pongs(10) {
    cartago.println("10 PONGS!!!");
}

rule +itemResource(string artifact_name, TYPE) : pongs(int X) {
    -+pongs(X+1);
    !itemProperty(artifact_name, URI, funct value);
    !ponged(F.valueAsString(value, 0));
}
```

The first rule stops the iterative behaviour when the argument of the `pongs` belief is `10`.  The other change is the addition of the `-+pongs(X+1)` which causes the argument to increment every time a `ping` message is received.

### Removing the hard coding of the Pinger URI

To remove the hard coding of the Pinger URI, one solution is to make use of one of the standard agent communication mechanisms which is based on messaging using FIPA ACL.  FIPA ACL communication is covered [here](http://not.defined).

The first step in implementing the communication is to create a shared language for the content of the messages passed. This is done though the creation of an additional ASTRA class, which we will call `Shared.astra`:

```
agent Shared {
    types communications {
        formula uri(string);
    }
}
```
This class will be incorporated into the ASTRA classes that define the agents that will interact with one another:

```
agent Main extends mams.MAMSAgent, Shared {
    ...
}
```

To incorporate the behaviour, we modify the `!main(...)` rule of the agent as follows:

```
rule +!main(list args) {
    MAMSAgent::!setup();

    S.createAgent("pinger", "Pinger");
}

rule @message(inform, "pinger", uri(string uri)) {
    S.createAgent("ponger", "Ponger");
    S.setMainGoal("ponger", [uri]);
}
```

The newly introduced rule is used to handle the receipt of the message from the `Pinger` agent.  Note the use of the `uri(...)` formula in the message event.

The `Shared` class is also associated with the `Pinger` class (because the `pinger` agent will send the message):

```
agent Pinger extends mams.javatypes.Passive, Shared {
    ...
}
```

Finally, we modify the `!init()` rule of the `Pinger` agent to look like this:

```
rule +!init() {
    MAMSAgent::!init();
    MAMSAgent::!created("base");

    Passive::!listResource(LIST_NAME, TYPE);

    !agentUri(string uri);
    send(inform, system.getOwner(), uri(uri));
}
```

The penultimate line retrieves the URI of the 'pinger' agent and the last line sends that URI to the `Main` agent.