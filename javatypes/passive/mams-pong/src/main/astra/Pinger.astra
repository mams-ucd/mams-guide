import com.fasterxml.jackson.databind.JsonNode;
import mams.web.HttpResponse;
import mams.web.HttpRequest;

agent Pinger extends mams.javatypes.Passive {
    module mams.JSONBuilder builder;

    constant string TYPE = "Message";
    constant string LIST_NAME = "pings";
    constant string URI = "uri";

    initial !init();

    rule +!init() {
        MAMSAgent::!init();
        MAMSAgent::!created("base");

        Passive::!listResource(LIST_NAME, TYPE);
    }

    // Got a PONG so PING...
	rule +itemResource(string artifact_name, TYPE) {
        !itemProperty(artifact_name, URI, funct value);
        !pinged(F.valueAsString(value, 0));
	}

    rule +!pinged(string pongerUri) :
            artifact(LIST_NAME, string artifact_name, cartago.ArtifactId id) &
            uri(artifact_name, string pinger_uri) {
        JsonNode node = builder.createObject();
        builder.addProperty(node, URI, pinger_uri);

        mams.MAMSAgent::!post(pongerUri,
            builder.toJsonString(node),
            HttpResponse response
        );

        if (httpUtils.hasCode(response, 201)) {
            string uri = httpUtils.header(response, "Location");
        } else {
            S.fail();
        }
    }
}