# mams-summer

This project demonstrates how to manipulate resources created by the agent. It is somewhat similar to the [mams-hello](../mams-hello/README.md) project but includes elements of the [mams-pong](../mams-pong/README.md) example (in terms of reacting to the creation of resources).

This example consists of 1 agent: a `Main` agent that creates a `sums` list. External systems can submit a `Sum` resource, which consists of two integer fields (`left` and `right`). When a new resource is created, the `Main` agent calculates the sum of the two numbers and stores it in a `total` field.

You can go back to the main page by clicking [here](../README.md).

## Project structure

This is a [Maven](https://maven.apache.org) project that uses the ASTRA Maven Plugin.  The project is described in the `pom.xml` file. The ASTRA source code (`Main.astra`) is in the `src/main/astra` folder, while the Java class `Message.java` used to specify the resources is in the `src/main/java` folder.


## Compiling and Running the code

To compile and run this example, you should be in the `mams-summer` folder.

To compile the code, type:

```
mvn compile
```

To run the code, type:

```
mvn astra:deploy
```

The initial output should look something like this:

```
[main]creating: main-base
Creating base artifact: main
Exposing Agent @ Base Uri: http://127.0.0.1:9000/main
```

To interact with the agent, send a POST request to the `/main/sums` endpoint:

```
POST http://localhost:9000/main/sums HTTP/1.1
Content-Type: application/json
Slug: sum1

{
    "left":4,
    "right":5
}
```

This causes the following line to be output:

```
[Web Server] Method: POST / Action: /main/sums
```

If you perform a GET request on the `/main/sums` endpoint, the response should look like this:

```
HTTP/1.1 200 OK
server: Netty
date: Sun, 19 May 2024 11:41:02 +0100
content-type: application/json
content-length: 32

[
  {
    "left": 4,
    "right": 5,
    "total": 9
  }
]
```

Both of these HTTP requests can be found in the `test.http` file.

You may have to use `CTRL-C` to stop the program after this line.

## Deconstructing the example

The code for this example is quite simple, and looks virtually identical to the [mams-hello](../mams-hello/README.md) example:

```
agent Main {
    constant string TYPE = "Sum";
    constant string LIST_NAME = "sums";

    rule +!main(list args) {
        // Setup the MAMS server on the default port (9000)
        MAMSAgent::!setup();

        // Create the agents base (body)
        MAMSAgent::!created("base");

        // Create the 'hello' list resource based on the Hello.java class.
        // The URL of this resource is: '/main/sums'
        Passive::!listResource(LIST_NAME, TYPE);
     }
}
```

The interesting part is the second rule:

```
rule +itemResource(string artifact_name, TYPE) {
    // Get the java object representation
    !getObject(artifact_name, Sum sum);

    // Perform the sum calculation
    int total = oa.getInt(sum, "left") + oa.getInt(sum, "right");

    // Store the sum in the object
    oa.set(sum, "total", total);

    // Update the resource
    !updateObject(artifact_name, sum);
}
```

This rule handles an event that is generated whenever a new passive resource is created. The `!getObject(...)` goal is provided by the MAMSAgent class and allows the agent to retrieve a Java object representation of the newly created resource based on the artifact name (`artifact_name`).

Once the agent has a reference to this object, it can start to access and manipulate the object as is necessary.  The next statement calculates the sum of the `left` and `right` fields using the [`ObjectAccess`](https://guide.astralanguage.com/en/latest/libraries/#627-astralangobjectaccess) module.  The third line then updates the object's `total` field.  The final line updates the resource using the modified object.