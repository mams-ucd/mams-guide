# mams-hello

This is the hello world project for MAMS that uses the Java Types library. It will demonstate how to create a MAMS agent and how to publish a basic resource using that library.

The example consists of a `Main` agent. This agent creates a passive list resource that is exposed at the `<name>/hello` endpoint. It supports only resources that match the `Hello.java` class. A `.http` file is provided to demonstrate how to interact with this resource.

You can go back to the main page by clicking [here](../README.md).

## Project structure

This is a [Maven](https://maven.apache.org) project that uses the ASTRA Maven Plugin.  The project is described in the `pom.xml` file. The ASTRA source code (`Main.astra`) is in the `src/main/astra` folder and the Java source code (`Hello.java`) is in the `src/main/java` folder.

A `test.http` file is provided in the root folder of the project that contains example HTTP Requests that can be executed when the program is running.


## Compiling and Running the code

To compile and run this example, you should be in the `mams-hello` folder.

To compile the code, type:

```
mvn compile
```

To run the code, type:

```
mvn astra:deploy
```

The expected output should look something like this:

```
[main]creating: main-base
Creating base artifact: main
Exposing Agent @ Base Uri: http://127.0.0.1:9000/main
```

## Interacting with the agent through HTTP

All the HTTP Requests described below can be found in the `test.http` file.  If you install the REST Client extension, you can execute them directly from that file. Alternatively, you can use a tool like PostMan to send the Requests.

### Viewing the available Hello Resources

To get the state of the Hello resource (which is a list of other resources), you can use a HTTP GET Request:

```
GET http://localhost:9000/main/hello HTTP/1.1
```

The expected (initial) response to this request is:

```
HTTP/1.1 200 OK
server: Netty
date: Tue, 16 Apr 2024 07:50:00 +0100
location: 
content-type: application/json
content-length: 2

[]
```

This output will change as resources are created.

### Creating a resource

Resources are created by sending a POST request to the `/main/hello` endpoint. The body of the request should contain the content being posted and should match the expected content.  In this example, the expected content is a JSON object containing a `name` property.  This matches the structure of the `Hello.java` class which has one field: a `String` identified by `name`.

An example HTTP POST Request is provided below (it is the second HTTP Request in the `test.http` file):

```
POST http://localhost:9000/main/hello HTTP/1.1
Content-Type: application/json
Slug: tester

{
    "name":"Tester"
}
```

Note that the request includes a `Slug` header that is used to indicate how the created resource should be dereferenced.  If such a header does not exist, you can either specify an identifier in the associated Java class (see below) or alternatively, a default naming scheme is used. To check out the default naming scheme, simply drop the `Slug` header and see what is returned.

The expected response is:

```
HTTP/1.1 201 Created
server: Netty
date: Tue, 16 Apr 2024 13:57:10 +0100
content-type: application/json
content-length: 0
Location: http://127.0.0.1:9000/main/hello/tester
```

Again, note that the response includes the location URI of the newly created resource. This is the only time that this information is shared when the basic Java Types library is used.  If you query the list resource, you do not get information about the URIs associated with the sub resources:

```
HTTP/1.1 200 OK
server: Netty
date: Tue, 16 Apr 2024 14:01:56 +0100
location: 
content-type: application/json
content-length: 19

[
  {
    "name": "Tester"
  }
]
```

### Using @Identifier with resources

In cases where the slug you would like to use is a natural part of the resource representation, you can make use of the `@Identifier` annotation. You annotate the field that you want to be used for the slug and drop the `Slug` header (you need to do this because the `Slug` header has precedence over other approaches).

To illustrate how this approach works, simply modify the `Hello.java` class in `src/main/java` as follows:

```
import mams.utils.Identifier;

public class Hello {
    @Identifier public String id;
    public String name;
}
```

Now, you can send a POST request with the following body:

```
POST http://localhost:9000/main/hello HTTP/1.1
Content-Type: application/json

{
    "id":"tester",
    "name":"Tester"
}

```

The result will be the same, but when you get the state of the `/main/hello` resource, the representation looks like this:

```
HTTP/1.1 200 OK
server: Netty
date: Tue, 16 Apr 2024 14:01:56 +0100
location: 
content-type: application/json
content-length: 28

[
  {
    "id": "tester",
    "name": "Tester"
  }
]
```

### Updating Resources

You cannot update a list resource directly, but you can update the resources that make up the list.  Given a resource `main/hello/tester`, you can update the state of the resource using a PUT request:

```
PUT http://localhost:9000/main/hello/tester HTTP/1.1
Content-Type: application/json

{
    "name":"Testee"
}
```

You can check out what happened by performing a GET request on the modified resource:

```
GET http://localhost:9000/main/hello/tester HTTP/1.1
```

If you attempt to do this for a resource where the `@Identifier` annotation is used, then any attempt to modify the identifier will result in an `500 Internal Server Error` response.

### DELETEing a resource

Finally, you can delete resources you create by using DELETE requests. For example, the request below deletes the tester resource we created earlier:

```
DELETE http://localhost:9000/main/hello/tester HTTP/1.1
```

You can check this out by sending a GET request to the `/main/hello` resource.  The subresource should no longer be present.