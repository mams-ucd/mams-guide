import com.fasterxml.jackson.databind.JsonNode;
import mams.web.HttpResponse;
import mams.web.HttpRequest;

agent Main extends mams.MAMSAgent {
    constant string RESOURCE_URI = "http://localhost:9000/main/hello";

    module mams.JSONBuilder builder;

    rule +!main(list args) {
        mams.MAMSAgent::!setup(9001);
        !read(RESOURCE_URI);

        !create(RESOURCE_URI, "Rem", "rem", string uri);
        !read(uri);

        !update(uri, "Bob");
        !read(uri);

        !delete(uri);
        !read(RESOURCE_URI);

        S.exit();
     }

    rule +!create(string target, string name, string slug, string uri) {
        JsonNode node = builder.createObject();
        builder.addProperty(node, "name", name);

        mams.MAMSAgent::!httpRequest(
            httpUtils.post(target, [header("Slug", slug)], builder.toJsonString(node)),
            HttpResponse response
        );

        if (httpUtils.hasCode(response, 201)) {
            uri = httpUtils.header(response, "Location");
        } else {
            S.fail();
        }
    }

    rule +!read(string uri) {
        mams.MAMSAgent::!get(uri, HttpResponse response);

        if (httpUtils.hasCode(response, 200)) {
            string content = httpUtils.bodyAsString(response);
            console.println("GET: " + uri);
            console.println(content);
        } else {
            S.fail();
        }
    }

    rule +!update(string target, string name) {
        JsonNode node = builder.createObject();
        builder.addProperty(node, "name", name);

        mams.MAMSAgent::!put(
            target, builder.toJsonString(node),
            HttpResponse response
        );

        if (~httpUtils.hasCode(response, 200)) {
            S.fail();
        }
    }
    
    rule +!delete(string target) {
        mams.MAMSAgent::!delete(target, HttpResponse response);
        if (~httpUtils.hasCode(response, 200)) {
            S.fail();
        }
    }
}