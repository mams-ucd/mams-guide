# mams-helloagent

This project demonstrates how to create an agent that can interact with a resource using HTTP.  Specifically, the agent provided in this project is able to interact with the MAMS Agent created in the [mams-hello](../mams-hello/README.md). 

The example consists of a `Main` agent that interacts with the hello agent via the passive list resource created in the first project.  The agent in this project performs various CRUD operations on the passive list resource.

You can go back to the main page by clicking [here](../README.md).

## Project structure

This is a [Maven](https://maven.apache.org) project that uses the ASTRA Maven Plugin.  The project is described in the `pom.xml` file. The ASTRA source code (`Main.astra`) is in the `src/main/astra` folder.


## Compiling and Running the code

To run this project, you must first run the [mams-hello](../mams-hello/README.md) project.

To compile and run this example, you should be in the `mams-helloagent` folder.

To compile the code, type:

```
mvn compile
```

To run the code, type:

```
mvn astra:deploy
```

The expected output should look something like this:

```
[main]GET: http://localhost:9000/main/hello
[main][]
[main]GET: http://137.43.154.89:9000/main/hello/rem
[main]{"name":"Rem"}
[main]GET: http://137.43.154.89:9000/main/hello/rem
[main]{"name":"Bob"}
CLOSED
```

## Deconstructing the example

All the code for this example can be found in the `Main.astra` file. The main behaviour is described in the main rule, which is given below:

```
constant string RESOURCE_URI = "http://localhost:9000/main/hello";

rule +!main(list args) {
  mams.MAMSAgent::!setup(9001);
  !read(RESOURCE_URI);

  !create(RESOURCE_URI, "Rem", "rem", string uri);
  !read(uri);

  !update(uri, "Bob");
  !read(uri);

  !delete(uri);
  !read(RESOURCE_URI);

  S.exit();
}
```

As can be seen in the above code, the example basically involves a series of Create-Read-Update-Delete (CRUD)-style operations that are executed on a given resource (identified by the constant RESOURCE_URI which is the URI of the list resource created in the `mams-hello` example).

### Reading a resource

This is implemented by a rule that is linked to the `+!read(...)` goal adoption event:

```
rule +!read(string uri) {
    mams.MAMSAgent::!get(uri, HttpResponse response);

    if (httpUtils.hasCode(response, 200)) {
        string content = httpUtils.bodyAsString(response);
        console.println("GET: " + uri);
        console.println(content);
    } else {
        S.fail();
    }
}
```

Reading a resource basically involves sending a `HTTP GET Request` to the URI of a resource requesting a representation of the current state of that resource. If the URI is correctly formed and the resource supports a GET request, then the expected behaviour is that it returns a 200 OK response where the body of the response contains the representation of the resource. The response is encoded as a Java Object that is an instance of the `mams.web.HttpResponse` class.  In the code above, the `HttpUtils` module, which is declared in the `MAMSAgent` agent class and associated with the module identifier `httpUtils`, is used to check the status code returned with the response.  If it is a `200` response, then the content is extracted from the body of the response and printed out to the console.  

To illustrate this, the first statement in the `+!main(...)` goal declares a `!read(...)` subgoal whose target URI is the list resource created by the `Main` agent in the `mams-hello` project.  The achievement of this goal generated the first two lines of the output:

```
[main]GET: http://localhost:9000/main/hello
[main][]
```

The second line of this output is the initial representation of the list, which is empty.

### Creating a resource

This example illustrates the creation of resources through the `!create(...)` subgoal. Specifically, it sends a post request to the `Main` agent of the `mams-hello` project, creating a `Hello` resource. The source code to achieve this is given below:

```
rule +!create(string target, string name, string slug, string uri) {
    JsonNode node = builder.createObject();
    builder.addProperty(node, "name", name);

    mams.MAMSAgent::!httpRequest(
        httpUtils.post(target, [header("Slug", slug)], builder.toJsonString(node)),
        HttpResponse response
    );

    if (httpUtils.hasCode(response, 201)) {
        uri = httpUtils.header(response, "Location");
    } else {
        S.fail();
    }
}
```

The `!create(...)` goal takes four parameters - the URI of the resource (in this case the list) that will create the new resource, some data to be used in constructing a representation of the resource to be created (in this case, a name), a slug that will be sent to give a preference for how the URI of the newly created resource will be generated, and an unbound parameter that will hold the URI of the newly created resource.

The rest of the code is split into 3 parts:

* the first two lines are used to create a JSON representation that will be sent in the body of the POST request.
* the next two lines declare a subgoal that will submit the HTTP request with the response being returned and associated with the response parameter (similarly to the way the GET request works).
* the last 5 lines check to see if the response has a successful response code (here a `201 Created` response). If so, it extracts the URI stored in the `Location` header of the response, otherwise it makes the plan fail.

An example of this is seen through the second statement in the `+!main(...)` rule:

```
  !create(RESOURCE_URI, "Rem", "rem", string uri);
  !read(uri);
```

The third statement then reads the state of the newly created resource.  The achievement of these goals generates the following lines in the output:

```
[main]GET: http://137.43.154.89:9000/main/hello/rem
[main]{"name":"Rem"}
```

Notice that the URI used is the URI of the newly created resource (which is a combination of the list URI with the slug appended on the end), and the state returned matches the input data (the name being `Rem`).

### Updating a resource

Updating works in a similar way to creating a resource, with the exception that the URI provided is the URI of the resource to be updated and that the only other information required is the new state of the resource (remember updating is often done by a PUT Request which replaces the old representation at the given URI with a new representation).  This is achieved through the following code:

```
rule +!update(string target, string name) {
    JsonNode node = builder.createObject();
    builder.addProperty(node, "name", name);

    mams.MAMSAgent::!put(
        target, builder.toJsonString(node),
        HttpResponse response
    );

    if (~httpUtils.hasCode(response, 200)) {
        S.fail();
    }
}
```

Here, the new representation is created in the same way as for the `!create(...)` goal, but the request is now achieved through a `!put(...)` goal.  The response should be a `200 OK` response and this is check in the last 3 lines of the rule.  Again, the plan fails if a 200 response is not provided.

An example of this subgoal in use is seen through the 4th and 5th statements of the `+!main(...)` rule:

```
!update(uri, "Bob");
!read(uri);
```

These statements generate the following output:

```
[main]GET: http://137.43.154.89:9000/main/hello/rem
[main]{"name":"Bob"}
```

Notice that the `name` property of the the resource representation has changed from `Rem` to `Bob`.

### Deleting a resource

The final mechanism demonstrated in this example is the one to delete a resource.  This is achieved through the following rule:

```
rule +!delete(string target) {
    mams.MAMSAgent::!delete(target, HttpResponse response);
    if (~httpUtils.hasCode(response, 200)) {
        S.fail();
    }
}
```

This rule simply sends a DELETE request to the given URI and monitors for a `200 Ok` response.  Once received, the object is removed. 

An example of this can be seen in the two penultimate statements in the `+!main(...)` rule:

```
!delete(uri);
!read(RESOURCE_URI);
```

The associated output is:

```
[main]GET: http://localhost:9000/main/hello
[main][]
```

Here, you can see that the rem resource no longer exists.