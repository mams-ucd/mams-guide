import astra.formula.Predicate;
import astra.formula.Formula;
import astra.core.Module;

public class TempStrings extends Module {
    @FORMULA
    public Formula contains(String text, String pattern) {
        return text.contains(pattern) ? Predicate.TRUE:Predicate.FALSE;
    }

}
