# mams-hello

This is the hello world project for MAMS that uses the Java Types library. It will demonstate how to create a MAMS agent and how to publish a basic resource using that library.

The example consists of a `Main` agent. This agent creates a passive list resource that is exposed at the `<name>/hello` endpoint. It supports only resources that match the `Hello.java` class. A `.http` file is provided to demonstrate how to interact with this resource.

You can go back to the main page by clicking [here](../README.md).

## Project structure

This is a [Maven](https://maven.apache.org) project that uses the ASTRA Maven Plugin.  The project is described in the `pom.xml` file. The ASTRA source code (`Main.astra`) is in the `src/main/astra` folder and the Java source code (`Hello.java`) is in the `src/main/java` folder.

A `test.http` file is provided in the root folder of the project that contains example HTTP Requests that can be executed when the program is running.


## Compiling and Running the code

To compile and run this example, you should be in the `mams-hello` folder.

To compile the code, type:

```
mvn compile
```

To run the code, type:

```
mvn astra:deploy
```

The expected output should look something like this:

```
[main]creating: main-base
Creating base artifact: main
Exposing Agent @ Base Uri: http://127.0.0.1:9000/main
```

## Interacting with the agent through HTTP

All the HTTP Requests described below can be found in the `test.http` file.  If you install the REST Client extension, you can execute them directly from that file. Alternatively, you can use a tool like PostMan to send the Requests.

### Viewing the available Hello Resources

To get the state of the Hello resource (which is a list of other resources), you can use a HTTP GET Request:

```
GET http://localhost:9000/main/hello HTTP/1.1
```

The expected (initial) response to this request is:

```
HTTP/1.1 200 OK
server: Netty
date: Mon, 23 Sep 2024 13:18:03 +0100
content-type: application/json
content-length: 11

{
  "name": ""
}
```

Now, go into the codebase and comment out the second rule (lines 23-26).

Re-run the code, send the GET request, and you should now get the following response:

```
HTTP/1.1 403 Forbidden
server: Netty
date: Mon, 23 Sep 2024 13:19:34 +0100
content-type: text/plain; charset=UTF-8
content-length: 9

Forbidden
```

### Changing the resource

To change the resource state, you need to send a PUT request to the `/main/hello` endpoint.  The body of the request should match the resource representation returned by the GET request above.

```
PUT http://localhost:9000/main/hello HTTP/1.1
Content-Type: application/json

{
    "name":"Testee"
}
```

The response you should receive to this request is 403 Forbidden.  

To make the agent process the request, you need to add another rule:

```
rule $cartago.signal(string AN, httpEvent(int index, "PUT"))
    : artifact("hello", AN, cartago.ArtifactId aid) {
  mams.javatypes.Active::!acceptRequest(index, AN);
}
```

The rule is basically the same as for the GET request, but with PUT instead.

Re-running the agent with this rule included and performing the PUT request should result in:

```
HTTP/1.1 200 OK
server: Netty
date: Mon, 23 Sep 2024 13:25:19 +0100
content-type: application/json
content-length: 0
```

If you follow this with a GET request, you should now see:

```
HTTP/1.1 200 OK
server: Netty
date: Mon, 23 Sep 2024 13:25:56 +0100
content-type: application/json
content-length: 17

{
  "name": "Testee"
}
```