# The Active Resource Model

The passive resource model is an implementation strategy for the [MAMS Java-Types library](/javatypes/) for the creation of resources in which the agent has direct contriol over the access to its external resources.

Each HTTP Request cached an a CArtAgO signal is emitted to the agent indicating that the request has been received.  The agent then has the option of either accepting or refusing the incoming request.

In addition, the agent receives the same notifications (events) whenever any of additional resources are created or updated.

## Examples

* [mams-hello-one](mams-hello-one): This is the hello world project for MAMS. It will demonstate how to create a MAMS agent and how to publish a basic JavaTypes-based resource using MAMS.
* [mams-hello](mams-hello): This is the hello world project for MAMS. It will demonstate how to create a MAMS agent and how to publish a JavaTypes-based list resource using MAMS.

## NOT UPDATED YET
* [mams-helloagent](mams-helloagent): This example builds on the previous example by introducing a second agent that interacts with the resources of the [mams-hello](mams-hello) agent. It will demonstate agent-agent interaction via MAMS resources.
* [mams-pong](mams-pong): This example uses the concepts learnt in the first two examples to implement a simple ping-pong scenario where two agents interact indirectly via `Ping` and `Pong` resources.
* [mams-summer](mams-summer): This example illustrates how the agent can update resources created via external HTTP requests. The example is an agent that can add two integer numbers together.

